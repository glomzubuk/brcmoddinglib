﻿using System;
using System.IO;
using UnityEngine;
using BepInEx.Logging;
using B83.Image;

namespace BRCML.Texture
{
    public class TextureUtils
    {
        private static ManualLogSource Logger = BRCModdingLib.Log;

        /// <summary>
        /// Loads a png from a file and returns it.
        /// </summary>
        /// <remarks>
        /// Loads the asset into memory, you should only load it once.
        /// Look into <see cref="TextureCache"/> if you need a class to manage that.
        /// </remarks>
        /// <returns>The loaded texture.</returns>
        /// <param name="file">The png file to load.</param>
        public static Texture2D LoadPNG(FileInfo file) 
        {
            if (!file.Exists)
            {
                Logger.LogWarning("LoadPNG: Could not find " + file.FullName);
                return null;
            }

            byte[] spriteBytes;
            using (var fileStream = file.OpenRead())
            using (var reader = new BinaryReader(fileStream))
            {
                var png = PNGTools.ReadPNGFile(reader);
                for (int i = png.chunks.Count - 1; i >= 0; i--)
                {
                    var c = png.chunks[i];
                    if (c.type == EPNGChunkType.gAMA ||
                        c.type == EPNGChunkType.pHYs ||
                        c.type == EPNGChunkType.sRBG)
                    {
                        png.chunks.RemoveAt(i);
                    }
                }
                using (var mem = new MemoryStream())
                {
                    PNGTools.WritePNGFile(png, new BinaryWriter(mem));
                    spriteBytes = mem.ToArray();
                }
            }

            string texName = Path.GetFileNameWithoutExtension(file.FullName);


            Texture2D tex = DefaultTexture();
            tex.name = texName;
            tex.LoadImage(spriteBytes); //This resizes the texture width and height

            return tex;
        }
        /// <inheritdoc cref="LoadPNG(FileInfo)"/>
        public static Texture2D LoadPNG(string filePath) => LoadPNG(new FileInfo(filePath));

        /// <summary>
        /// Returns a default texture with optional size.
        /// </summary>
        /// <returns>The texture.</returns>
        /// <param name="width">New texture's width.</param>
        /// <param name="height">New texture's height.</param>
        public static Texture2D DefaultTexture(int width = 512, int height = 512)
        {
            return new Texture2D(width, height, TextureFormat.RGBA32, true, false)
            {
                filterMode = FilterMode.Trilinear,
                anisoLevel = 9,
                mipMapBias = -0.5f,
            };
        }
    }
}
