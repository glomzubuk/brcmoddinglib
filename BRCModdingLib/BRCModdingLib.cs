﻿using System;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using BRCML.Utils;

namespace BRCML
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInProcess("Bomb Rush Cyberfunk.exe")]
    public class BRCModdingLib : BaseUnityPlugin
    {
        internal static BRCModdingLib Instance { get; private set; } = null;
        internal static ManualLogSource Log { get; private set; } = null;
        internal static ConfigFile Conf { get; private set; } = null;

        void Awake()
        {
            Instance = this;
            Log = Logger;
            Logger.LogInfo("Hello, world!");
            ModdingFolder.InitConfig(this.Config);
        }
    }
}
