﻿using System.Reflection;
using BRCML;

#region Assembly attributes
[assembly: AssemblyVersion(PluginInfos.PLUGIN_VERSION)]
[assembly: AssemblyTitle(PluginInfos.PLUGIN_NAME + " (" + PluginInfos.PLUGIN_ID + ")")]
[assembly: AssemblyProduct(PluginInfos.PLUGIN_NAME)]
#endregion

namespace BRCML
{
    public static class PluginInfos
    {
        public const string PLUGIN_NAME = "BRCModdingLib";
        public const string PLUGIN_ID = "fr.glomzubuk.plugins.brc.brcml";
        public const string PLUGIN_VERSION = "0.2.0";
    }
}
