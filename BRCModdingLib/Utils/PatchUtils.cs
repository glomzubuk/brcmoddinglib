﻿using System;
using System.Linq;
using System.Collections.Generic;
using HarmonyLib;
using BepInEx.Logging;

namespace BRCML.Utils
{
    /// <summary>
    /// A class containing helper methods for harmony patches
    /// </summary>
    public static class PatchUtils
    {
        /// <summary>
        /// Logs all the instructions to either the provided log, or LLBML's one.
        /// </summary>
        /// <param name="instructions">The instructions to log.</param>
        /// <param name="from">The start index from which to start logging.</param>
        /// <param name="to">The end index where to stop logging.</param>
        /// <param name="logSource">The log source to use.</param>
        public static void LogInstructions(IEnumerable<CodeInstruction> instructions, int from = 0, int to = -1, ManualLogSource logSource = null)
        {
            if (logSource == null) logSource = BRCModdingLib.Log;

            int i = 0;
            if (to == -1) to = instructions.Count();
            foreach (var inst in instructions)
            {
                if (i >= from && i <= to)
                    logSource.LogDebug(i + ": " + inst);
                i++;
            }
        }


        /// <summary>
        /// Logs a single instruction.
        /// </summary>
        public static void LogInstruction(CodeInstruction ci)
        {
            BRCModdingLib.Log.LogDebug($"OpCode: {ci.opcode} | Operand: {ci.operand} {(ci.operand != null ? $"-> {ci.operand.GetType()}" : "NULL")}");
        }
    }
}
