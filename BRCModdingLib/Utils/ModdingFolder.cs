﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;


namespace BRCML.Utils
{

    /// <summary>
    /// Handles the paths for centralising user-supplied information.
    /// </summary>
    public static class ModdingFolder
    {
        private static readonly ManualLogSource Logger = BRCModdingLib.Log;

        private static string DefaultModdingFolderPath => Path.Combine(BepInEx.Paths.GameRootPath, "ModdingFolder");
        //private static string DefaultModdingFolderPath => Path.Combine(Path.GetDirectoryName(LLBMLPlugin.Instance.Info.Location), "ModdingFolder");

        private static ConfigEntry<string> moddingFolderPath;
        private static ConfigEntry<bool> doneTutorial;

        internal static void InitConfig(ConfigFile config)
        {
            moddingFolderPath = config.Bind<string>(
                "ModdingFolder",
                "ModdingFolderPath",
                DefaultModdingFolderPath, 
                new ConfigDescription(
                    "The path of the folder that will be used to store mod's user provided information.",
                    null,
                    "modmenu_directorypicker"
                )
            );
            doneTutorial = config.Bind<bool>(
                "ModdingFolder",
                "DoneTutorial",
                false,
                new ConfigDescription(
                    "Whether or not the user was guided on using the modding folder.",
                    null,
                    "modmenu_hidden"
                )
            );
            Directory.CreateDirectory(moddingFolderPath.Value);
        }

        /// <summary>
        /// Gets the mod's own subfolder located within the user-defined modding folder.
        /// </summary>
        /// <param name="modInfo">The mod to get the subfolder for.</param>
        /// <returns>The mod's subfolder.</returns>
        public static DirectoryInfo GetModSubFolder(PluginInfo modInfo)
        {
            return GetModdingFolder().CreateSubdirectory(modInfo.Metadata.Name);
        }

        /// <summary>
        /// Gets the user-defined modding folder.
        /// </summary>
        /// <returns>The folder that contains all mods user-supplied data.</returns>
        public static DirectoryInfo GetModdingFolder()
        {
            DirectoryInfo moddingFolder = new DirectoryInfo(moddingFolderPath.Value);
            if (!moddingFolder.Exists) {
                if (moddingFolderPath.Value == DefaultModdingFolderPath)
                {
                    moddingFolder.Create();
                }
                else
                {
                    Logger.LogError("[ModdingFolder] Invalid path: " + moddingFolder.FullName);
                    return null;
                }
            }
            return moddingFolder;
        }
    }
}
