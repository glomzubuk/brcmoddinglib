﻿using System;
using BRCML.Math;

namespace BRCML.Utils
{
    public abstract class Hash<T> : IEquatable<T>
    {
        protected static int HashLength = 16;
        public byte[] Bytes { get; protected set; }


        public Hash()
        {
            this.Bytes = new byte[HashLength];
            this.Bytes.Initialize();
        }
        protected Hash(byte[] bytes)
        {
            if (bytes.Length != HashLength) throw new NotSupportedException();
            this.Bytes = bytes;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that contains the hex representation of the current <see cref="Hash{T}"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="Hash{T}"/>.</returns>
        public override string ToString()
        {
            return StringUtils.BytesToHexString(Bytes);
        }

        public static explicit operator byte[] (Hash<T> a) => a.Bytes;
        public abstract bool Equals(T other);

#pragma warning disable 1591
        public override bool Equals(object obj)
        {
            if (obj != null && obj is Hash<T>)
            {
                Hash<T> shs = (Hash<T>)obj;
                return this.Equals(shs);
            }
            return false;
        }
        public bool Equals(byte[] other) => BinaryUtils.ByteArraysEqual(this.Bytes, other);
        public bool Equals(Hash<T> other) => BinaryUtils.ByteArraysEqual(this.Bytes, other.Bytes);

        public override int GetHashCode()
        {
            return Bytes.GetHashCode();
        }

        public static bool operator ==(Hash<T> hash1, Hash<T> hash2) => hash1.Equals(hash2);
        public static bool operator !=(Hash<T> hash1, Hash<T> hash2) => !(hash1 == hash2);
        public static bool operator ==(Hash<T> hash1, byte[] hash2) => hash1.Equals(hash2);
        public static bool operator !=(Hash<T> hash1, byte[] hash2) => !(hash1 == hash2);
        public static bool operator ==(byte[] hash1, Hash<T> hash2) => hash2.Equals(hash1);
        public static bool operator !=(byte[] hash1, Hash<T> hash2) => !(hash1 == hash2);
#pragma warning restore 1591
    }
}
