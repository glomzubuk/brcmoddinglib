﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Reptile;

namespace BRCML.Utils
{
    /// <summary>
    /// A class to hold <see cref="string"/> related functions.
    /// </summary>
    public static class StringUtils
    {
        /*
        /// <summary>
        /// Mapping from <see cref="Characters"/> to <see cref="string"/> for character names.
        /// </summary>
        public static Dictionary<Characters, string> characterNames = new Dictionary<Characters, string>
        {
            [Characters.angel] = "Raptor",
            [Character.ROBOT] = "Switch",
            [Character.CANDY] = "Candyman",
            [Character.ELECTRO] = "Grid",
            [Character.BOOM] = "Sonata",
            [Character.CROC] = "Latch",
            [Character.PONG] = "Dice",
            [Character.BOSS] = "Doombox",
            [Character.COP] = "Nitro",
            [Character.SKATE] = "Jet",
            [Character.GRAF] = "Toxic",
            [Character.BAG] = "Dust&Ashes",
        };


        /// <summary>
        /// Mapping from <see cref="Stage"/> to <see cref="string"/> for regular stages names.
        /// </summary>
        public static Dictionary<Stage, string> regularStagesNames = new Dictionary<Stage, string>
        {
            [Stage.city] = "City",
            [Stage.] = "Sewers",
            [Stage.JUNKTOWN] = "Desert",
            [Stage.CONSTRUCTION] = "Elevator",
            [Stage.FACTORY] = "Factory",
            [Stage.SUBWAY] = "Subway",
            [Stage.STADIUM] = "Stadium",
            [Stage.STREETS] = "Streets",
            [Stage.POOL] = "Pool",
            [Stage.ROOM21] = "Room21",
        };
        */


        /// <summary>
        /// Converts a byte to a binary string representation.
        /// </summary>
        /// <param name="input">The byte to convert.</param>
        public static string ByteToString(byte input)
        {
            return Convert.ToString(input, 2).PadLeft(8, '0');
        }

        /// <summary>
        /// Converts a byte array to a binary string representation array.
        /// </summary>
        /// <param name="input">The byte array to convert.</param>
        public static string[] BytesToStrings(byte[] input)
        {
            return input.Select(ByteToString).ToArray();
        }

        /// <summary>
        /// Converts a byte array to a hex string representation array.
        /// </summary>
        /// <param name="input">The byte array to convert.</param>
        public static string BytesToHexString(byte[] input)
        {
            return BitConverter.ToString(input).Replace("-", "");
        }

        /// <summary>
        /// Converts a bytes to a binary string representation.
        /// </summary>
        /// <param name="input">The byte array to convert.</param>
        public static string PrettyPrintBytes(byte[] input)
        {
            return string.Join(" ", BytesToStrings(input));
        }
    }
}
