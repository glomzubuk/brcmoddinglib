﻿using System;
using System.Reflection;
using System.Collections.Generic;
using BepInEx;
using BepInEx.Logging;
using HarmonyLib;

namespace BRCML.Utils
{
    /// <summary>
    /// A class that helps abstracting soft dependencies make maintaining compatibility with LLBMM easier
    /// </summary>
    public static class ModDependenciesUtils
    {
        private static readonly ManualLogSource Logger = BRCModdingLib.Log;

        /// <summary>
        /// Returns whether or not the mod was present in the BepInEx Chainloader.
        /// </summary>
        /// <returns><c>true</c>, if mod was present, <c>false</c> otherwise.</returns>
        /// <param name="modGUID">Mod GUID.</param>
        public static bool IsModPresent(string modGUID)
        {
            return BepInEx.Bootstrap.Chainloader.PluginInfos.ContainsKey(modGUID);
        }

        /// <summary>
        /// Returns whether or not the mod was present in the BepInEx Chainloader.
        /// </summary>
        /// <returns><c>true</c>, if mod was present, <c>false</c> otherwise.</returns>
        /// <param name="modGUID">Mod GUID.</param>
        public static bool IsModLoaded(string modGUID)
        {
            if (IsModPresent(modGUID))
            {
                BaseUnityPlugin plugin = BepInEx.Bootstrap.Chainloader.PluginInfos[modGUID]?.Instance;
                if (plugin != null)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
