﻿using System;
namespace BRCML.Utils
{
    public interface IByteable
    {
        byte[] ToBytes();
    }
}
