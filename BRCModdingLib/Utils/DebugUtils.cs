﻿using System;
using System.Collections.Generic;
using System.Linq;
using BepInEx.Logging;
using HarmonyLib;

namespace BRCML.Utils
{
    /// <summary>
    /// A class containing helper methods for debbuging game behaviors
    /// </summary>
    public static class DebugUtils
    {
        /// <summary>
        /// Prints the current stacktrace.
        /// </summary>
        public static void PrintStacktrace()
        {
            BRCModdingLib.Log.LogInfo("---------------------------- Stacktrace: \n" +
                new System.Diagnostics.StackTrace());
        }
    }
}
